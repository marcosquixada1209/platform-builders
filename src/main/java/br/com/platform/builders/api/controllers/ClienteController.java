package br.com.platform.builders.api.controllers;

import java.util.Optional;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.platform.builders.api.models.Cliente;
import br.com.platform.builders.api.repositories.ClienteRepository;

@RestController
public class ClienteController {
	
	private static final Logger log = LoggerFactory.getLogger(ClienteController.class);

	@Autowired
	private ClienteRepository clienteRepository;

	/**
	 * Retorna a listagem de todos clientes.
	 * 
	 * @return Iterable<Cliente>
	 */
	@RequestMapping(value = "/clientes", method = RequestMethod.GET, produces = "application/json")
	public Iterable<Cliente> Get(Pageable pageable) {
		log.info("Buscando todos Clientes.");
		return clienteRepository.findAll(pageable);
	}

	/**
	 * Retorna um cliente por ID.
	 * 
	 * @param id
	 * @return ResponseEntity<Cliente>
	 */
	@RequestMapping(value = "/cliente/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Cliente> GetById(@PathVariable(value = "id") long id) {
		log.info("Buscando um cliente por id: {}", id);
		Optional<Cliente> cliente = clienteRepository.findById(id);
		if (cliente.isPresent())
			return new ResponseEntity<Cliente>(cliente.get(), HttpStatus.OK);
		else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	/**
	 * Retorna Clientes por CPF ou Nome
	 * 
	 * @param Cliente
	 * @return Iterable<Cliente>
	 */
	@RequestMapping(value = "/cliente", method = RequestMethod.GET, produces = "application/json")
	public Iterable<Cliente> GetByCpfNome(@RequestBody Cliente cliente) {
		log.info("Buscando um cliente por cpf: {} e nome: {}", cliente.getCpf(), cliente.getNome());
		Iterable<Cliente> list = () -> StreamSupport.stream(clienteRepository.findAll().spliterator(), false)
		        .filter(c -> c.getCpf().equals(cliente.getCpf()) && c.getNome().contains(cliente.getNome()))
		        .iterator();
		
		return list;
	}

	/**
	 * Salva um cliente no banco de dados.
	 * 
	 * @param Cliente
	 * @return Iterable<Cliente>
	 */
	@RequestMapping(value = "/cliente", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public Cliente Post(@RequestBody Cliente cliente) {
		log.info("Salvando um cliente no banco de dados: {}", cliente);
		return clienteRepository.save(cliente);
	}

	/**
	 * Atualiza um cliente no banco de dados.
	 * 
	 * @param id
	 * @param Cliente
	 * @return ResponseEntity<Cliente>
	 */
	@RequestMapping(value = "/cliente/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
	public ResponseEntity<Cliente> Put(@PathVariable(value = "id") long id, @RequestBody Cliente newCliente) {
		log.info("Atualizando um cliente no banco de dados: {}, id: {}", newCliente, id);
		Optional<Cliente> oldCliente = clienteRepository.findById(id);
		if (oldCliente.isPresent()) {
			Cliente cliente = oldCliente.get();
			cliente.setNome(newCliente.getNome());
			clienteRepository.save(cliente);
			return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	/**
	 * Atualiza parcialmente um cliente no banco de dados.
	 * 
	 * @param Cliente
	 * @param id
	 * @return ResponseEntity<Cliente>
	 */
	@RequestMapping("/cliente/{id}")
	public ResponseEntity<Cliente> Patch(@RequestBody Cliente partialUpdate, @PathVariable("id") long id) {
		log.info("Atualizando parcialmente um cliente no banco de dados: {}, id: {}", partialUpdate, id);
		Optional<Cliente> oldCliente = clienteRepository.findById(id);
		if (oldCliente.isPresent()) {
			Cliente cliente = oldCliente.get();
			clienteRepository.save(partialUpdate);
			return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	/**
	 * Deleta um cliente do banco de dados.
	 * 
	 * @param id
	 * @return ResponseEntity<Object>
	 */
	@RequestMapping(value = "/cliente/{id}", method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<Object> Delete(@PathVariable(value = "id") long id) {
		log.info("Deletando um cliente no banco de dados, id: {}", id);
		Optional<Cliente> cliente = clienteRepository.findById(id);
		if (cliente.isPresent()) {
			clienteRepository.delete(cliente.get());
			return new ResponseEntity<>(HttpStatus.OK);
		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
}