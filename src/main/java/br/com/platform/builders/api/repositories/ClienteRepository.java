package br.com.platform.builders.api.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import br.com.platform.builders.api.models.Cliente;

@Repository
public interface ClienteRepository extends PagingAndSortingRepository<Cliente, Long> { }